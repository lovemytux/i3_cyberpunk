; vim: ft=dosini
[colors]
black = #18262F
red = #ef5253
green = #7cc844
yellow = #e4b51c
orange = #c75000
blue = #33b5e1
magenta = #a363d5
cyan = #52cbb0
white = #f5f7fa
primary = #33b5e1
secondary = #52cbb0
danger = #ef5253
background = #0a18262f
foreground = #a6afb8

[sweet]
#bg = #161925
bg = #000000
bg1 = #181B28
bg2 = #1A1D2B
bg3 = #171E27
fg = #C3C7D1
#fg = #F600FF
fg1 = #F3E9FD
fg2 = #ffffff
#green = #00b300 #this is the actual green 
liquid= #00A790
purple = #F600FF
green = #1DB954
orange = #C75000
primary = #C75000
white = #ffffff
black = #000000
#primary = #C50ED2

[bar/top]
monitor = ${env:MONITOR:HDMI1}
width = 100%
height = 30
offset-x = 0
offset-y = 0
;background = #00f0f0f
background = ${sweet.bg}
foreground = ${sweet.fg}

underline-size = 0
overline-size = 0
font-0 = "Hack:size=10;3"
font-1 = "Font Droid:size=10;3"
font-2 = "FontAwesome:pixelsize=12;3"
font-3 = "Font Awesome 4 Free Solid:size=10;3"
font-4 = "Font Awesome 4 Free:size=10;3"

modules-left = spotifyStatus 
;title
modules-center = wm
modules-right = ram cpu keyboard time volume
;pdl kbd

fixed-center = true

tray-position = right
tray-background = ${sweet.bg}
tray-padding = 5
; tray-transparent = true

[module/pdl]
type               = custom/text
content            = |
content-foreground = #000f0f0f
content-background = #000f0f0f
content-padding = 24

[module/plctl]
type = custom/menu
format-padding = 2

label-open = ""
label-close = "  "
label-separator = "  "
format-foreground = ${sweet.fg}
format-background = ${sweet.bg}

menu-0-0 = 
menu-0-0-exec = playerctl previous
menu-0-1 = 
menu-0-1-exec = playerctl stop
menu-0-2 = 
menu-0-2-exec = playerctl next

[module/playing]
type = custom/script
exec = nowplay
interval = 1
click-left = playerctl play-pause
format-background = ${sweet.bg}
format-foreground = ${sweet.fg}
format-padding = 2

[module/kbd]
type = internal/xkeyboard
format = <label-indicator>

label-indicator = %name:0:4:%
label-indicator-padding = 2
label-indicator-foreground = ${sweet.fg}
label-indicator-background = ${sweet.bg}

[module/volume]
type = internal/pulseaudio
use-ui-max = true
format-volume = <ramp-volume> <label-volume>

format-volume-background = ${sweet.bg}
format-volume-foreground = ${sweet.fg}
format-volume-padding = 1

label-volume-foreground = ${sweet.fg}

;label-muted =  OFF
label-muted = OFF
label-muted-foreground = ${sweet.fg}
label-muted-background = ${sweet.purple}
label-muted-padding = 1

ramp-volume-foreground = ${sweet.fg}
ramp-volume-0 = 
ramp-volume-1 = 
ramp-volume-2 = 

[module/title]
type = internal/xwindow
label = %title:0:45:%
label-maxlen = 45
label-foreground = ${sweet.fg}
label-background = ${sweet.bg}
label-padding = 3

[module/ram]
type = internal/memory
interval = 3

label = %gb_free%
label-padding = 1
label-foreground = ${sweet.fg}
label-background = ${sweet.bg}
format-prefix = 
format-prefix-padding = 1
format-prefix-foreground = ${sweet.fg}
format-prefix-background = ${sweet.bg}

[module/cpu]
type = internal/cpu
format-prefix = 
format-prefix-padding = 1
format-prefix-foreground = ${sweet.fg}
format-prefix-background = ${sweet.bg}
format-background = ${sweet.bg}
interval = 1
format = <ramp-coreload>
format-padding = 2
ramp-coreload-0 = ▁
ramp-coreload-1 = ▂
ramp-coreload-2 = ▃
ramp-coreload-3 = ▄
ramp-coreload-4 = ▅
ramp-coreload-5 = ▆
ramp-coreload-6 = ▇
ramp-coreload-7 = █
ramp-coreload-0-foreground = ${colors.green}
ramp-coreload-1-foreground = ${colors.green}
ramp-coreload-2-foreground = ${colors.yellow}
ramp-coreload-3-foreground = ${colors.yellow}
ramp-coreload-4-foreground = ${colors.orange}
ramp-coreload-5-foreground = ${colors.orange}
ramp-coreload-6-foreground = ${colors.red}
ramp-coreload-7-foreground = ${colors.red}

[module/wm]
type = internal/i3
enable-click = true
enable-scroll = true
wrapping-scroll = false
strip-wsnumbers = true

label-mode-background = ${sweet.bg}
label-mode-foreground = ${sweet.fg}
label-mode-padding    = 3

label-focused            = %name%
#label-focused-background = ${sweet.primary}
label-focused-background = ${sweet.purple}
label-focused-foreground = ${sweet.black}
label-focused-padding    = 3

label-unfocused            = %name%
label-unfocused-background = ${sweet.bg}
label-unfocused-foreground = ${sweet.liquid}
label-unfocused-padding    = 3

label-visible            = %name%
label-visible-background = ${sweet.bg}
label-visible-foreground = ${sweet.liquid}
label-visible-padding    = 3

label-urgent            = %name%
label-urgent-background = ${sweet.bg}
label-urgent-foreground = ${colors.red}
label-urgent-padding    = 3

[module/keyboard]
type = internal/xkeyboard
format-prefix = 
format-prefix-padding = 1

; List of indicators to ignore
blacklist-0 = num lock
blacklist-1 = scroll lock
format-foreground = ${sweet.fg}
format-background = ${sweet.bg}

[module/time]
type = internal/date
interval = 30

format =  <label>
format-padding = 1
format-foreground = ${sweet.fg}
format-background = ${sweet.bg}

label = %date%
;label-padding = 1
label-foreground = ${sweet.fg}

date = %H:%M
date-alt = %d %b %Y

[module/battery]
type = internal/battery
battery = BAT0
adapter = AC0
full-at = 0

format-charging-padding = 2
format-discharging-padding = 2
format-full-padding = 2

format-charging = <animation-charging><label-charging>
format-charging-foreground = ${sweet.fg}
format-charging-background = ${sweet.bg}

format-discharging = <ramp-capacity><label-discharging>
format-discharging-foreground = ${sweet.fg}
format-discharging-background = ${sweet.bg}

format-full-prefix = 
format-full-prefix-padding = 1
format-full-foreground = ${sweet.fg}
format-full-background = ${sweet.bg}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 
ramp-capacity-foreground = ${sweet.fg}
ramp-capacity-background = ${sweet.bg}
ramp-capacity-padding = 1

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-4 = 
animation-charging-padding = 1
animation-charging-framerate = 500

[module/spotifyStatus]
format-padding = 1
type = custom/script
exec = ~/.config/polybar/spotifyS
exec-if = pgrep spotify > /dev/null
interval = 2.0
click-left = playerctl -p spotify previous
click-middle = playerctl -p spotify play-pause
click-right = playerctl -p spotify next
label = "%output%"
content = 
format-foreground = ${sweet.bg}
format-background = ${sweet.green}
label-indicator-foreground = ${sweet.fg}
label-indicator-background = ${sweet.bg}

[module/checkVPN]
format-padding = 1
type = custom/script
exec = ~/.config/polybar/checkVPN.sh
interval = 1.0
label = "%output%"
format-foreground = ${sweet.purple}
format-background = ${sweet.bg}
